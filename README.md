#ifndef CWICZENIA_TICTACTOE_H
#define CWICZENIA_TICTACTOE_H


class tictactoe {
    enum pole {kolo, krzyz, puste};
    pole tab[3][3];
    void rysunek();
    bool sprawdzenie(pole &wygrane);
    void wprowadzenie(pole znak);
    void czyszczenie();

public:
    tictactoe();

    void gra();
};


#endif //CWICZENIA_TICTACTOE_H
